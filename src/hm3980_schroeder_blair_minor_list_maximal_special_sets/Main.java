/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hm3980_schroeder_blair_minor_list_maximal_special_sets;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Charlie
 */
public class Main {

    public static File globalFile;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String [] args) {
        
        File fileName = new File (args[0]);
        
        try {
            Scanner sc = new Scanner(fileName);
            ScanMaximalSets.ScanMaximalSets(sc);
            globalFile = fileName;
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
