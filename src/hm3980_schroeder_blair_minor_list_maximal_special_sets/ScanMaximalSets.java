/*
 * Student Names: Charlie Schroedr, Jake Blair, Daniel Minor
 * Due Date: March 6th
 * Homework #: 3980-1
 * Problem: list maximal special sets of (0,1) data set
 * Lower bound on the number of maximal sets: 
 * The source code in this project is the work of our group only.
 * CS, JB, DM
 */
package hm3980_schroeder_blair_minor_list_maximal_special_sets;

import hm3980_schroeder_blair_minor_list_maximal_special_sets.MaximalSet;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Charlie
 */
public class ScanMaximalSets {
    
    public static int kThreshold = 5;
    public static List maximalSets = new ArrayList();
    public static int [] kMap;
    public static int kMapSize;
    public static int [][] dataSet = new int [100][100];
    public static int [] searchSet;
    
    public static void ScanMaximalSets(Scanner sc){
        
        fillArray(sc);
        
        int [] totals = new int [100];
        
        totals = searchForKValues();
        
        meetsThreshold(totals);
        checkTwoSets();
        
        
        
        for(int i = 3; i <= kMapSize; i++)
        {
            List tempSearchSet = new ArrayList();
            for(int j = 0; j < i; j++)
            {
                tempSearchSet.add(0);
            }
            
            findLargeSets(i,i,-1, tempSearchSet);
        }
        
        for(int i = 0; i < maximalSets.size(); i++)
        {
           MaximalSet ms = (MaximalSet) maximalSets.get(i);
           ms.printElements();
        }
        
    }
    
    /**
     * This function totals up the number of times individual objects are purchased.
     * @param sc
     * @return 
     */
    
    public static int [] searchForKValues()
    {
        int [] totals = new int [100];
        
        for(int i = 0; i < 100; i++)
        {
            for(int j =0; j < 100; j++)
            {
                if(dataSet[i][j] == 1)
                {
                    totals[j]++;
                }
            }
        }
        
        return totals;
    }
    
    /**
     * This function checks to see if the objects have been bought enough times
     * to meet or exceed the threshold of interest, kThreshold.
     * If they do meet it, they are declared so in the array of booleans.
     * @param totals
     * @return 
     */
    
    public static void meetsThreshold(int [] totals)
    {
        boolean [] sufficientK = new boolean [100];
        
        int sizeCount = 0;
        for(int i =0; i < 100; i++)
        {
            if(totals[i] >= kThreshold)
            {
                sufficientK[i] = true;
                sizeCount++;
            } else
            {
                sufficientK[i] = false;
            }
        }
        
        kMap = new int [sizeCount];
        kMapSize = sizeCount;
        int index = 0;
        for(int k = 0; k < 100 ; k++)
        {
            if(sufficientK[k] == true)
            {
                kMap[index] = k;
                //System.out.println(kMap[index] + " " + k);
                index++;
            }
        }
        
        //System.out.println(kMap.length);
    }

    public static void checkTwoSets() 
    {
       for(int i = 0; i < (kMapSize - 1); i++)
       {
           for(int j = i+1; j < kMapSize; j++)
           {
               searchSet = new int [2];
               searchSet[0] = kMap[i];
               searchSet[1] = kMap[j];
               checkTransaction(searchSet);
           }
       }
    }
    
    public static void fillArray(Scanner sc)
    {
        for(int i =0; i < 100; i++)
        {
            for(int j =0; j < 100; j++)
            {
                dataSet[i][j] = Integer.parseInt(sc.next());
            }
        }
    }
    
    public static void checkTransaction(int [] searchSet)
    {
        int found = 0;
        for(int i = 0; i < 100; i++)
        {
            if((dataSet[i][searchSet[0]] == 1) && (dataSet[i][searchSet[1]] == 1))
            {
                found++;
            }
        }
        if(found >= kThreshold)
        {
            MaximalSet ms = new MaximalSet();
            ms.setSize(2);
            ms.setElements(searchSet);
            maximalSets.add(ms);
        }
    } 
    
    public static void findLargeSets(int size, int current, int lastIter, List tempSearchSet)
    {
                
        for(int i = lastIter + 1; i < kMapSize - current; i++)
        {
            tempSearchSet.set(size - current, kMap[i]);
            
            if(current == 1)
            {
                
                if(checkSuperSet(tempSearchSet))
                {
                    //System.out.println(tempSearchSet.toString());
                    //System.out.println("SearchSet is a Superset");
                }
                else
                {
                    System.out.println("CheckingTransactionLarger");
                    System.out.println(tempSearchSet.toString());
                    checkTransactionLarger(tempSearchSet);
                    //break;
                }
            }
            else
            {
                int temp = current - 1;
                
                findLargeSets(size, temp, i, tempSearchSet);
            }
        }
        
    }
    
    public static boolean checkSuperSet(List tempSearchSet)
    {
        int counter = 0;
        for(int i = 0; i < maximalSets.size(); i++)
        {
            int temp;
            MaximalSet newMS = (MaximalSet) maximalSets.get(i);
            temp = newMS.getSize();
            
            for(int j = 0; j < temp; j++)
            {
                for(int k = 0; k < tempSearchSet.size(); k++)
                {
                    newMS.printElements();
                    System.out.println(tempSearchSet.get(k));
                    
                    if(newMS.getElement(j) == (Integer) tempSearchSet.get(k))
                    {
                        System.out.println("Match");
                        counter++;
                    }
                }
            }
            
            if(counter == newMS.getSize())
            {
                return true;
            }
            counter = 0;
        }
        System.out.println("returnedfalse!");
        return false;
    } 
    
    public static void checkTransactionLarger(List tempSearchSet)
    {
        int foundElement = 0;
        int foundSet = 0;
        
        for(int j = 0; j < 100; j++)
        {
            for(int i = 0; i < tempSearchSet.size(); i++)
            {
                if(1 == dataSet[j][(Integer) tempSearchSet.get(i)])
                {
                    foundElement++;
                }
            }
            if(foundElement == tempSearchSet.size())
            {
                foundSet++;
            }
            foundElement = 0;
        }
        if(foundSet >= kThreshold)
        {
            MaximalSet ms = new MaximalSet();
            ms.setSize(tempSearchSet.size());
            int [] tempArray = new int [tempSearchSet.size()];
            
            for(int i = 0; i < tempSearchSet.size(); i++)
            {
                tempArray[i] = (Integer) tempSearchSet.get(i);
            }
            
            ms.setElements(tempArray);
            maximalSets.add(ms);
        }
    }
    
}
