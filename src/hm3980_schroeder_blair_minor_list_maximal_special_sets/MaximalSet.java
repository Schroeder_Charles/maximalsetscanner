/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hm3980_schroeder_blair_minor_list_maximal_special_sets;

/**
 *
 * @author Charlie
 */
public class MaximalSet {

    private int size;
    private int [] elements;
    
    public int getSize ()
    {
        return this.size;
    }
        
    public void setSize(int size)
    {
        this.size = size;
        this.elements = new int [size];
    }
    
    public void setElements(int [] newElements)
    {
        System.arraycopy(newElements, 0, this.elements, 0, this.size);
    }
    
    public void printElements()
    {
        System.out.print("(");
        
        for(int i = 0; i < this.size; i++)
        {
            System.out.print(this.elements[i] + ", ");
        }
        
        System.out.println(")");
    }
    
    public int getElement(int index)
    {
        return this.elements[index];
    }
}
